// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:0,dpts:2,wrdp:False,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:True,fgod:False,fgor:False,fgmd:0,fgcr:0,fgcg:0,fgcb:0,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:True,fnfb:True,fsmp:False;n:type:ShaderForge.SFN_Final,id:4795,x:33103,y:32635,varname:node_4795,prsc:2|emission-4872-OUT,alpha-3373-OUT;n:type:ShaderForge.SFN_Tex2d,id:6074,x:32471,y:32384,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:_MainTex,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:2393,x:32768,y:32697,varname:node_2393,prsc:2|A-6074-RGB,B-5394-OUT,C-797-RGB,D-9248-OUT;n:type:ShaderForge.SFN_VertexColor,id:2053,x:32235,y:33153,varname:node_2053,prsc:2;n:type:ShaderForge.SFN_Color,id:797,x:32235,y:32930,ptovrint:True,ptlb:Color,ptin:_TintColor,varname:_TintColor,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.3474265,c2:0.9264706,c3:0.90251,c4:1;n:type:ShaderForge.SFN_Vector1,id:9248,x:32235,y:33081,varname:node_9248,prsc:2,v1:2;n:type:ShaderForge.SFN_Time,id:298,x:30278,y:32038,varname:node_298,prsc:2;n:type:ShaderForge.SFN_Multiply,id:9909,x:31090,y:32488,varname:node_9909,prsc:2|A-298-T,B-855-OUT;n:type:ShaderForge.SFN_Slider,id:855,x:30724,y:32645,ptovrint:False,ptlb:Wave Speed,ptin:_WaveSpeed,varname:_WaveSpeed,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-3,cur:-0.2,max:3;n:type:ShaderForge.SFN_Add,id:8300,x:31326,y:32488,varname:node_8300,prsc:2|A-2304-Y,B-9909-OUT;n:type:ShaderForge.SFN_TexCoord,id:1438,x:31052,y:32167,varname:node_1438,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Multiply,id:2046,x:31515,y:32488,varname:node_2046,prsc:2|A-8300-OUT,B-2292-OUT;n:type:ShaderForge.SFN_Slider,id:2292,x:31139,y:32755,ptovrint:False,ptlb:Segments,ptin:_Segments,varname:_Segments,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-10,cur:4,max:10;n:type:ShaderForge.SFN_Frac,id:6080,x:31697,y:32488,varname:node_6080,prsc:2|IN-2046-OUT;n:type:ShaderForge.SFN_Power,id:6894,x:31875,y:32505,varname:node_6894,prsc:2|VAL-6080-OUT,EXP-1521-OUT;n:type:ShaderForge.SFN_Slider,id:1521,x:31482,y:32735,ptovrint:False,ptlb:Power of Waves,ptin:_PowerofWaves,varname:_PowerofWaves,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:3,max:3;n:type:ShaderForge.SFN_Clamp01,id:5394,x:32343,y:32511,varname:node_5394,prsc:2|IN-1725-OUT;n:type:ShaderForge.SFN_Multiply,id:3373,x:32746,y:32894,varname:node_3373,prsc:2|A-6074-A,B-2053-A;n:type:ShaderForge.SFN_Multiply,id:4872,x:32929,y:32697,varname:node_4872,prsc:2|A-2393-OUT,B-3373-OUT;n:type:ShaderForge.SFN_Multiply,id:8351,x:30548,y:31979,varname:node_8351,prsc:2|A-2572-OUT,B-298-TSL;n:type:ShaderForge.SFN_ValueProperty,id:2572,x:30299,y:31893,ptovrint:False,ptlb:Noise Time,ptin:_NoiseTime,varname:node_2572,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:2;n:type:ShaderForge.SFN_Frac,id:6394,x:30746,y:32001,varname:node_6394,prsc:2|IN-8351-OUT;n:type:ShaderForge.SFN_Multiply,id:5331,x:30979,y:32033,varname:node_5331,prsc:2|A-6394-OUT,B-5875-OUT,C-2572-OUT;n:type:ShaderForge.SFN_ValueProperty,id:5875,x:30764,y:32205,ptovrint:False,ptlb:Noise Posterize,ptin:_NoisePosterize,varname:node_5875,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:300;n:type:ShaderForge.SFN_Round,id:5780,x:31145,y:32033,varname:node_5780,prsc:2|IN-5331-OUT;n:type:ShaderForge.SFN_Add,id:8991,x:31355,y:32020,varname:node_8991,prsc:2|A-5780-OUT,B-1438-UVOUT;n:type:ShaderForge.SFN_Posterize,id:6590,x:31557,y:32033,varname:node_6590,prsc:2|IN-8991-OUT,STPS-5875-OUT;n:type:ShaderForge.SFN_Noise,id:6381,x:31755,y:32064,varname:node_6381,prsc:2|XY-6590-OUT;n:type:ShaderForge.SFN_Add,id:1725,x:32134,y:32511,varname:node_1725,prsc:2|A-3290-OUT,B-6894-OUT;n:type:ShaderForge.SFN_Multiply,id:3290,x:32012,y:32321,varname:node_3290,prsc:2|A-6381-OUT,B-8577-OUT;n:type:ShaderForge.SFN_Slider,id:8577,x:31679,y:32348,ptovrint:False,ptlb:Noise Power,ptin:_NoisePower,varname:node_8577,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.2,max:1;n:type:ShaderForge.SFN_FragmentPosition,id:2304,x:30892,y:32281,varname:node_2304,prsc:2;proporder:6074-797-855-2292-1521-2572-5875-8577;pass:END;sub:END;*/

Shader "Shader Forge/Hologram Shader" {
    Properties {
        _MainTex ("MainTex", 2D) = "white" {}
        _TintColor ("Color", Color) = (0.3474265,0.9264706,0.90251,1)
        _WaveSpeed ("Wave Speed", Range(-3, 3)) = -0.2
        _Segments ("Segments", Range(-10, 10)) = 4
        _PowerofWaves ("Power of Waves", Range(0, 3)) = 3
        _NoiseTime ("Noise Time", Float ) = 2
        _NoisePosterize ("Noise Posterize", Float ) = 300
        _NoisePower ("Noise Power", Range(0, 1)) = 0.2
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One One
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _TintColor;
            uniform float _WaveSpeed;
            uniform float _Segments;
            uniform float _PowerofWaves;
            uniform float _NoiseTime;
            uniform float _NoisePosterize;
            uniform float _NoisePower;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float4 vertexColor : COLOR;
                UNITY_FOG_COORDS(2)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float4 node_298 = _Time;
                float2 node_6590 = floor((round((frac((_NoiseTime*node_298.r))*_NoisePosterize*_NoiseTime))+i.uv0) * _NoisePosterize) / (_NoisePosterize - 1);
                float2 node_6381_skew = node_6590 + 0.2127+node_6590.x*0.3713*node_6590.y;
                float2 node_6381_rnd = 4.789*sin(489.123*(node_6381_skew));
                float node_6381 = frac(node_6381_rnd.x*node_6381_rnd.y*(1+node_6381_skew.x));
                float node_3373 = (_MainTex_var.a*i.vertexColor.a);
                float3 emissive = ((_MainTex_var.rgb*saturate(((node_6381*_NoisePower)+pow(frac(((i.posWorld.g+(node_298.g*_WaveSpeed))*_Segments)),_PowerofWaves)))*_TintColor.rgb*2.0)*node_3373);
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,node_3373);
                UNITY_APPLY_FOG_COLOR(i.fogCoord, finalRGBA, fixed4(0,0,0,1));
                return finalRGBA;
            }
            ENDCG
        }
    }
    CustomEditor "ShaderForgeMaterialInspector"
}
