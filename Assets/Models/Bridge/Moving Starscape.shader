// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:32990,y:32689,varname:node_3138,prsc:2|emission-4824-RGB;n:type:ShaderForge.SFN_Tex2d,id:4824,x:32679,y:32705,ptovrint:False,ptlb:Moving Texture,ptin:_MovingTexture,varname:node_4824,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:a13a7f47295fef84a91e06f3df23b6cf,ntxv:2,isnm:False|UVIN-7981-UVOUT;n:type:ShaderForge.SFN_Panner,id:7981,x:32449,y:32695,varname:node_7981,prsc:2,spu:0.1,spv:1|UVIN-7410-UVOUT,DIST-3377-OUT;n:type:ShaderForge.SFN_TexCoord,id:7410,x:32225,y:32624,varname:node_7410,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Slider,id:8513,x:32005,y:32906,ptovrint:False,ptlb:Pan Speed,ptin:_PanSpeed,varname:node_8513,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-1,cur:-0.1,max:1;n:type:ShaderForge.SFN_Time,id:8288,x:32005,y:32988,varname:node_8288,prsc:2;n:type:ShaderForge.SFN_RemapRange,id:2457,x:32285,y:32975,varname:node_2457,prsc:2,frmn:0,frmx:1,tomn:-1,tomx:1|IN-8288-T;n:type:ShaderForge.SFN_Multiply,id:3377,x:32500,y:32872,varname:node_3377,prsc:2|A-8513-OUT,B-2457-OUT;proporder:4824-8513;pass:END;sub:END;*/

Shader "Shader Forge/Moving Starscape" {
    Properties {
        _MovingTexture ("Moving Texture", 2D) = "black" {}
        _PanSpeed ("Pan Speed", Range(-1, 1)) = -0.1
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _MovingTexture; uniform float4 _MovingTexture_ST;
            uniform float _PanSpeed;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos( v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float4 node_8288 = _Time;
                float2 node_7981 = (i.uv0+(_PanSpeed*(node_8288.g*2.0+-1.0))*float2(0.1,1));
                float4 _MovingTexture_var = tex2D(_MovingTexture,TRANSFORM_TEX(node_7981, _MovingTexture));
                float3 emissive = _MovingTexture_var.rgb;
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
