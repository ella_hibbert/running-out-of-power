﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Scenario
{
    public ECManual.Lists.ProblemListRoot rootProblem;
    public List<ECManual.Lists.ProblemListProblem> shipProblems;

    public Scenario(ECManual.Lists.ProblemListRoot root, List<ECManual.Lists.ProblemListProblem> shipProblems)
    {
        rootProblem = root;
        this.shipProblems = shipProblems;
    }
    
    public void DialInteraction(DialComponent component)
    {
        Debug.Log("A dial was changed to " + component.GetState());
    }

    public void SwitchInteraction(SwitchComponent component)
    {
        Debug.Log("A switch was changed to " + component.GetState());
    }

    public void SliderInteraction(SliderComponent component)
    {
        Debug.Log("A slider was changed to " + component.GetState());
    }

    public void ButtonInteraction(ButtonComponent component)
    {
        Debug.Log("A button was pressed");
    }










    //public ShipData GetShipData()
    //{
    //    return dataManager.GetShipData();
    //}

    //public string GetDiagnosticStartupMessage()
    //{
    //    return diagnostics.GetStartUpMessage();
    //}

    //public string GetDiagnosticResults(string code)
    //{
    //    return diagnostics.GetResults(code);
    //}

    //public string HandlePassword(string password)
    //{
    //    Debugger.Log("Got keyword " + password + ", checking for a solution to match...");

    //    Solution associatedSolution = null;
    //    SolutionStep associatedStep = null;
    //    string response = "";

    //    foreach (var solution in incompleteSolutions)
    //    {
    //        foreach (var step in solution.steps)
    //        {
    //            if (step.terminalPassword == password)
    //            {
    //                associatedSolution = solution;
    //                associatedStep = step;
    //                break;
    //            }
    //        }
    //    }

    //    if (associatedSolution != null && associatedStep != null)
    //    {
    //        Debugger.Log("The terminal password " + password + " belongs to the solution " + associatedSolution.action);
    //        associatedStep.completed = true;

    //        bool allStepsComplete = true;
    //        int numberOfIncompleteSteps = 0;

    //        foreach (var step in associatedSolution.steps)
    //        {
    //            if (!step.completed)
    //            {
    //                numberOfIncompleteSteps++;
    //                allStepsComplete = false;
    //            }
    //        }

    //        if (allStepsComplete)
    //        {
    //            Debugger.Log("Completed all steps of solution " + associatedSolution.action);
    //            response = "\n" + associatedSolution.action + " AT 100% READY, INITIALISING\nSYSTEMS STABILISING";
    //            incompleteSolutions.Remove(associatedSolution);

    //            // check if this solution's problem has any knockon problems which will also be solved now
    //            if (associatedSolution.problem.parent != null)
    //            {
    //                foreach (var problem in associatedSolution.problem.parent.knockons)
    //                {
    //                    if (problem.solution.action == SolutionAction.FIX_PARENT_PROBLEM)
    //                    {
    //                        Debugger.Log("Fixed " + problem.cause + " too because it was a knockon which required its parent to be fixed");
    //                        incompleteSolutions.Remove(problem.solution);
    //                        // check if to hide the effect warning
    //                    }
    //                }
    //            }
    //        }
    //        else
    //        {
    //            Debugger.Log("Not yet completed all steps");
    //            response = "\n" + associatedSolution.action + " AT " + ((float)numberOfIncompleteSteps / associatedSolution.steps.Count) * 100 + "%, REQUIRES FURTHER ACTION";
    //        }
    //    }

    //    if (incompleteSolutions.Count == 0)
    //    {
    //        response += "\nALL PROBLEMS RESOLVED, SHIP STABLE\nGOOD JOB";
    //    }

    //    return response;
    //}
}
