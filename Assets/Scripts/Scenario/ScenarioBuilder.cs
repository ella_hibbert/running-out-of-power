﻿using ECManual.Lists;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ScenarioBuilder
{
    private ProblemList problemDefinitions;

    //private Problem rootProblem;
    //private int numberOfProblems;
    
    //private List<Problem> problems = new List<Problem>();
    //private List<Solution> solutions = new List<Solution>();
    //private Diagnostics diagnostics;

    //private DataManager dataManager;
    //private ProblemManager problemManager;
    //private SolutionManager solutionManager;
    //private DoorManager doorManager;

    private MersenneTwister random;

    public ScenarioBuilder(string password, ProblemList problems)
    {
        int seed = GetSeed(password);
        problemDefinitions = problems;
        //dataManager = new DataManager(seed, shipData);
        //problemManager = new ProblemManager(seed);
        //solutionManager = new SolutionManager(seed, dataManager);
        //doorManager = new DoorManager(seed, dataManager);
        //diagnostics = new Diagnostics(seed);
        
        random = new MersenneTwister(seed);
    }
    
    public Scenario GenerateScenario()
    {
        Debug.Log("CREATING THE SCENARIO");

        var root = problemDefinitions.RootProblems[random.Next(problemDefinitions.RootProblems.Length)];

        Debug.Log("Chose root: " + root.Name);

        var shipProblems = new List<ProblemListProblem>();
        var possibleKnockons = new List<string> (root.Knockons);
        var mainKnockon = possibleKnockons[random.Next(possibleKnockons.Count)];

        Debug.Log("Main knockon from root: " + mainKnockon);

        var mainKnockonProblem = problemDefinitions.ShipProblems.First(x => x.Name == mainKnockon);
        shipProblems.Add(mainKnockonProblem);
        Debug.Log("Found knockon definition for " + mainKnockonProblem.Name);

        possibleKnockons = new List<string>(mainKnockonProblem.Knockons);
        int numberOfKnockons = random.Next(2, 4);

        Debug.Log("Choosing " + numberOfKnockons + " knockons...");

        for (int i = 0; i < numberOfKnockons; i++)
        {
            Debug.Log("Knockons to choose from: " + possibleKnockons.Count);
            var randomKnockon = possibleKnockons[random.Next(possibleKnockons.Count)];
            possibleKnockons.Remove(randomKnockon);
            var randomKnockonProblem = problemDefinitions.ShipProblems.First(x => x.Name == randomKnockon);
            shipProblems.Add(randomKnockonProblem);

            Debug.Log("Chose knockon: " + randomKnockonProblem.Name);
        }

        return new Scenario(root, shipProblems);

        //GenerateProblems();
        //GenerateCauseAndEffects();
        //GenerateSolutions();
        ////LockDoors();
        //GenerateEffectWarnings();
        //GenerateDiagnostics();
        
        //return new Scenario(dataManager, diagnostics, solutions);
    }

    //private void GenerateProblems()
    //{
    //    numberOfProblems = random.Next(3, 6);

    //    rootProblem = problemManager.GetRandomRootProblem();
    //    Debugger.Log("Root problem: " + rootProblem.cause + " originating in the " + rootProblem.associatedRoom + " room");

    //    problems = new List<Problem>();
    //    problems.Add(rootProblem);
    //    Problem currentProblem = rootProblem;

    //    int maxNumberOfTries = 100;
    //    int tries = 0;

    //    while (problems.Count < numberOfProblems)
    //    {
    //        if (tries >= maxNumberOfTries)
    //        {
    //            Debugger.Log("Max number of tries exceeded");
    //            break;
    //        }

    //        Problem knockon = problemManager.GetKnockonProblem(currentProblem);

    //        if (knockon.cause == Cause.NONE)
    //        {
    //            Debugger.Log(currentProblem.cause + " has no other knockon problems, going back to check random previous problem");
    //            if (problems.Count > 1)
    //            {
    //                currentProblem = problems[random.Next(0, problems.Count - 1)];
    //            }
    //        }
    //        else
    //        {
    //            Debugger.Log(currentProblem.cause + " caused knockon of " + knockon.cause + " in the " + knockon.associatedRoom + " room");
    //            currentProblem.knockons.Add(knockon);
    //            problems.Add(knockon);
    //            currentProblem = knockon;
    //            Debugger.Log("New current problem: " + currentProblem.cause);
    //        }

    //        tries++;
    //    }

    //    Debugger.Log("Finished generating problems");
    //}

    //private void GenerateCauseAndEffects()
    //{
    //    Debugger.Log("Generating effects of the " + problems.Count + " problems");

    //    foreach (var problem in problems)
    //    {
    //        Debugger.Log("Getting effect for " + problem.cause);

    //        Effect randomEffect = problemManager.GetRandomEffect(problem.cause);

    //        Debugger.Log(problem.cause + " had the effect of " + randomEffect);
    //        problem.effect = randomEffect;
    //    }

    //    Debugger.Log("Finished generating effects");
    //}

    //private void GenerateSolutions()
    //{
    //    Debugger.Log("");
    //    Debugger.Log("Generating solutions for the problems");

    //    foreach (var problem in problems)
    //    {
    //        Solution solution = solutionManager.GetRandomSolution(problem);

    //        string steps = "";

    //        foreach (var step in solution.steps)
    //        {
    //            steps += step.type + " at " + step.targetIdentifier;

    //            if (step.type == SolutionStepType.TERMINAL_PASSWORD)
    //            {
    //                steps += " with password " + step.terminalPassword;
    //            }
    //            else if (step.type == SolutionStepType.CONTROL_BUTTON)
    //            {
    //                steps += " needing button " + step.requiredButton;
    //            }
    //            else if (step.type == SolutionStepType.CONTROL_SLIDERS)
    //            {
    //                steps += " needing values ";

    //                foreach (var slider in step.requiredSliderValues)
    //                {
    //                    steps += "(" + slider.Value + " for " + slider.Key + ")";
    //                }
    //            }
    //            else if (step.type == SolutionStepType.CONTROL_TOGGLES)
    //            {
    //                steps += " needing toggles";
    //                foreach (var toggle in step.requiredOnToggles)
    //                {
    //                    steps += " " + toggle;
    //                }
    //            }

    //            steps += ", ";
    //        }

    //        Debugger.Log(" - " + solution.action + " to fix " + problem.cause + " which requires steps " + steps);

    //        solution.problem = problem;
    //        solutions.Add(solution);
    //    }

    //    Debugger.Log("Finished generating effects and solutions");
    //    Debugger.Log("");
    //}

    //private void LockDoors()
    //{
    //    doorManager.LockBreachDoors();

    //    int randomDoorsToLock = random.Next(1, dataManager.GetNumberOfDoors() / 4); // lock no more than a quarter of all doors

    //    Debugger.Log("Randomly locking at most " + randomDoorsToLock + " doors:");

    //    for (int i = 0; i < randomDoorsToLock; i++)
    //    {
    //        doorManager.LockRandomDoor();
    //    }
    //}

    //private void GenerateEffectWarnings()
    //{
    //    Debugger.Log("Generating the warnings");

    //    string warnings = "";
    //    List<Effect> allEffects = new List<Effect>();

    //    foreach (var problem in problems)
    //    {
    //        if (!allEffects.Contains(problem.effect))
    //        {
    //            allEffects.Add(problem.effect);
    //        }
    //    }

    //    foreach (var effect in allEffects)
    //    {
    //        warnings += "WARNING: " + effect.ToString().Replace('_', ' ') + "\n";
    //    }

    //    dataManager.SetTerminalWarnings(warnings);
    //}

    //private void GenerateDiagnostics()
    //{
    //    diagnostics.Generate();
    //}

    private int GetSeed(string password)
    {
        string seed = "";

        foreach (var ch in password)
        {
            int index = ch % 32;
            Debug.Log("Index of " + ch + ": " + index);
            seed += index;
        }

        return (int)long.Parse(seed);
    }
}