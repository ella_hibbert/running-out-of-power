﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartMenuController : MonoBehaviour {

    public TMP_InputField passwordInput;
    public Button validatebutton;

    void Start()
    {
        passwordInput.onValueChanged.AddListener(delegate { CheckPassword(passwordInput.text); });
    }

    public void CheckPassword(string password)
    {
        if (password.Length == 6)
        {
            validatebutton.gameObject.SetActive(true);
        } else
        {
            validatebutton.gameObject.SetActive(false);
        }
    }

    public void Validate()
    {
        PlayerPrefs.SetString("password", passwordInput.text);
        SceneManager.LoadScene(1);
    }

}
