﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public enum MovementDir
    {
        LEFT,
        RIGHT,
        UP,
        DOWN,
        NONE
    }

    private Rigidbody rb;
    private float walkingSpeedAcc = 0.5f;
    private float maxWalkingSpeed = 5f;
    private bool canAccelerate = true;
    private bool stationary = true;

    private KeyCode lastKeyPressed;

    private List<KeyCode> keysDown = new List<KeyCode>();
    
    public MovementDir moveDirection = MovementDir.NONE;
    
    void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    public void DetectDirection()
    {
        if (!Input.anyKey && !stationary)
        {
            stationary = true;
            moveDirection = MovementDir.NONE;
            keysDown = new List<KeyCode>();
            rb.velocity = Vector3.zero;
            canAccelerate = true;
            return;
        }

        // handle key downs
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            keysDown.Insert(0, KeyCode.LeftArrow);
            stationary = false;
            ChangeDirection();
        }

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            keysDown.Insert(0, KeyCode.RightArrow);
            stationary = false;
            ChangeDirection();
        }

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            keysDown.Insert(0, KeyCode.UpArrow);
            stationary = false;
            ChangeDirection();
        }

        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            keysDown.Insert(0, KeyCode.DownArrow);
            stationary = false;
            ChangeDirection();
        }

        // handle key ups
        if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            if (moveDirection == MovementDir.LEFT)
            {
                ChangeDirection();
            }

            keysDown.Remove(KeyCode.LeftArrow);
        }

        if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            if (moveDirection == MovementDir.RIGHT)
            {
                ChangeDirection();
            }

            keysDown.Remove(KeyCode.RightArrow);
        }

        if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            if (moveDirection == MovementDir.UP)
            {
                ChangeDirection();
            }

            keysDown.Remove(KeyCode.UpArrow);
        }

        if (Input.GetKeyUp(KeyCode.DownArrow))
        {
            if (moveDirection == MovementDir.DOWN)
            {
                ChangeDirection();
            }

            keysDown.Remove(KeyCode.DownArrow);
        }

        if (rb.velocity.magnitude > maxWalkingSpeed)
        {
            canAccelerate = false;
        }

        // choose which direction to move in based on what keys are down
        if (keysDown.Count > 0)
        {
            // get the first in the list (ie the most recent press)
            // and move in that direction
            if (keysDown[0] == KeyCode.LeftArrow)
            {
                moveDirection = MovementDir.LEFT;
            }
            else if (keysDown[0] == KeyCode.RightArrow)
            {
                moveDirection = MovementDir.RIGHT;
            }
            else if (keysDown[0] == KeyCode.UpArrow)
            {
                moveDirection = MovementDir.UP;
            }
            else if (keysDown[0] == KeyCode.DownArrow)
            {
                moveDirection = MovementDir.DOWN;
            }
            else
            {
                Debug.Log("What...");
                // should be no other options?
            }
        }
    }
    
	public void Move () {
        
        if (moveDirection == MovementDir.LEFT)
        {
            if (canAccelerate)
            {
                rb.velocity += new Vector3(0, 0, 1) * walkingSpeedAcc;
            }
        }

        if (moveDirection == MovementDir.RIGHT)
        {
            if (canAccelerate)
            {
                rb.velocity += new Vector3(0, 0, -1) * walkingSpeedAcc;
            }
        }

        if (moveDirection == MovementDir.UP)
        {
            if (canAccelerate)
            {
                rb.velocity += new Vector3(1, 0, 0) * walkingSpeedAcc;
            }
        }

        if (moveDirection == MovementDir.DOWN)
        {
            if (canAccelerate)
            {
                rb.velocity += new Vector3(-1, 0, 0) * walkingSpeedAcc;
            }
        }
    }

    private void ChangeDirection()
    {
        canAccelerate = true;
        rb.velocity = Vector3.zero;
    }
}
