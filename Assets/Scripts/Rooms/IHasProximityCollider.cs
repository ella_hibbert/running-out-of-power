﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHasProximityCollider {
    
    void HandleProximityCollisionEnter();

    void HandleProximityCollisionExit();

    void Use();

    void HandleInput();

    Transform GetKeyboardPromptLocation();
}
