﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum RoomType
{
    CRYO,
    REACTOR,
    BRIDGE,
    SHIELD,
    OXYGEN,
    CORRIDOR
}

public class Room : MonoBehaviour {

    public Transform cameraPosition;

    public Transform leftCameraBoundary;
    public Transform rightCameraBoundary;

    public string identifier;
    public RoomType type;

    public Door[] doors;

    [HideInInspector]
    public GameObject geometry;

    void Awake()
    {
        geometry = transform.Find("Geometry").gameObject;
        doors = GetComponentsInChildren<Door>();

        if (leftCameraBoundary == null || rightCameraBoundary == null)
        {
            leftCameraBoundary = rightCameraBoundary = cameraPosition;
        }

        var controller = GameObject.Find("Controller").GetComponent<Controller>();

        foreach (var door in doors)
        {
            door.Init(controller);
        }
    }
    
}
