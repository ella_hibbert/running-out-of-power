﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorLockedIndicator : MonoBehaviour {

    private Material mat;
    public Color unlocked;
    public Color locked;

    public void Init()
    {
        mat = GetComponent<MeshRenderer>().material;
    }

    public void Unlock()
    {
        mat.color = unlocked;
    }

    public void Lock()
    {
        mat.color = locked;
    }
}
