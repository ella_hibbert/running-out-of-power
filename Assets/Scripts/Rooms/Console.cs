﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Console : MonoBehaviour, IHasProximityCollider {

    private Controller controller;
    public ConsoleOverlay console;

    void Start()
    {
        controller = FindObjectOfType<Controller>();
    }

    public Transform GetKeyboardPromptLocation() { return null; }

    public void HandleInput() { }

    public void HandleProximityCollisionEnter()
    {
        controller.PlayerInProximityTo(this, transform);
    }

    public void HandleProximityCollisionExit()
    {
        controller.PlayerInProximityTo(null, null);
    }

    public void Use()
    {
        if (console.IsVisible())
        {
            console.Hide();
        } else
        {
            console.Show();
        }
    }
}
