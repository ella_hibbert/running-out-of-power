﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProximityCollider : MonoBehaviour {

    private IHasProximityCollider obj;

    void Awake()
    {
        obj = transform.parent.GetComponent<IHasProximityCollider>();
    }
    
    void OnTriggerEnter(Collider other)
    {
        obj.HandleProximityCollisionEnter();
    }

    void OnTriggerExit(Collider other)
    {
        obj.HandleProximityCollisionExit();
    }
}
