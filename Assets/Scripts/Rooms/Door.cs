﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour, IHasProximityCollider {
    
    [SerializeField]
    private Door connectingDoor;

    [SerializeField]
    private bool locked = false;

    private Controller controller;
    private Room room;
    
    private Transform spawnLocation;
    private Transform keyboardPromptLocation;
    private DoorLockedIndicator lockedIndicator;
    
    public string identifier;

    public void Init(Controller controller)
    {
        this.controller = controller;
        room = transform.parent.parent.GetComponent<Room>();
        spawnLocation = transform.Find("Spawn Location");
        keyboardPromptLocation = transform.Find("Keyboard Prompt Location");
        lockedIndicator = transform.GetComponentInChildren<DoorLockedIndicator>();

        lockedIndicator.Init();
        
        if (connectingDoor == null)
        {
            Debug.Log("Connecting door is null for " + identifier + " in " + room.name);
        }
        
        if (locked)
        {
            Lock();
        } else
        {
            Unlock();
        }
    }

    public void HandleProximityCollisionEnter()
    {
        controller.PlayerInProximityTo(this, transform);
    }

    public void HandleProximityCollisionExit()
    {
        controller.PlayerInProximityTo(null, null);
    }

    public void Use()
    {
        if (!locked)
        {
            controller.MoveToRoom(connectingDoor.GetRoom(), connectingDoor.spawnLocation);
        }
    }

    public void HandleInput() { }
    
    public Room GetConnectedRoom()
    {
        return connectingDoor.GetRoom();
    }

    public Transform GetKeyboardPromptLocation()
    {
        return keyboardPromptLocation;
    }

    public Transform GetSpawnPoint()
    {
        return spawnLocation;
    }

    public Transform GetConnectedSpawnPoint()
    {
        return connectingDoor.spawnLocation;
    }

    public Room GetRoom()
    {
        if (room == null)
        {
            room = transform.parent.parent.GetComponent<Room>();
        }
        
        return room;
    }
    
    public bool IsLocked()
    {
        return locked;
    }

    public void Lock()
    {
        locked = true;
        lockedIndicator.Lock();
    }

    public void Unlock()
    {
        locked = false;
        lockedIndicator.Unlock();
    }
}
