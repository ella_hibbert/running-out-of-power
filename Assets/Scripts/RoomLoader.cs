﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomLoader : MonoBehaviour {

    private Room[] rooms;

    public void Init(Room currentRoom)
    {
        rooms = FindObjectsOfType<Room>();
        SetCurrentRoom(currentRoom);
    }

    public void SetCurrentRoom(Room currentRoom)
    {
        // set all rooms as inactive
        foreach (var room in rooms)
        {
            room.geometry.SetActive(false);
        }

        // enable the current room and all of its neighbours
        currentRoom.geometry.SetActive(true);

        foreach (var door in currentRoom.doors)
        {
            door.GetConnectedRoom().geometry.SetActive(true);
        }
    }

}
