﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Panel : MonoBehaviour {

    public PanelType type;
    public PanelComponent[] components;

    private int quantity;
    
    public void Init()
    {
        components = GetComponentsInChildren<PanelComponent>();
    }

    public string GetName()
    {
        return name.Split(' ')[0];
    }

    public int GetQuantity()
    {
        if (quantity == 0)
        {
            quantity = int.Parse(name.Split(' ')[1]);
        }

        return quantity;
    }

}

public enum PanelType
{
    BUTTONS,
    SWITCHES,
    SLIDERS,
    DIALS,
    //GAUGES,
    //LIGHTS
}
