﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PanelDatabase : MonoBehaviour {

    public Panel[] panels;

    public Panel GetPanel(ECManual.ConsoleComponentType type, int variant)
    {
        var panelName = type.ToString() + " " + variant;
        
        return panels.FirstOrDefault(x => x.name.ToUpper() == panelName.ToUpper());
    }
}
