﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SliderComponent : PanelComponent
{
    public Transform leftLimit;
    public Transform rightLimit;
    private SliderKnob knob;

    public float percent;

    public void InitSymptom(ECManual.Symptom symptom, ECManual.ConsoleComponent component, bool isSymptomatic)
    {
        knob = GetComponentInChildren<SliderKnob>();
        knob.Init(this);
        
        Init(component.label);

        var sliderComponent = component as ECManual.SliderComponent;
        var max = sliderComponent.max;
        var min = sliderComponent.min;
        var state = float.Parse(symptom.state);
        var pc = (state - min) / (max - min);

        // need to set the labels too

        if (isSymptomatic)
        {
            switch (symptom.condition.ToUpper())
            {
                case "EQUAL":
                    SetKnob(pc);
                    break;
                case "GREATER THAN":
                    SetKnob(Random.Range(pc, 1f));
                    break;
                case "LESS THAN":
                    SetKnob(Random.Range(0f, pc));
                    break;
            }
        } else
        {
            switch (symptom.condition.ToUpper())
            {
                case "EQUAL":
                    if (Random.Range(0f, 1f) > 0.5f)
                    {
                        SetKnob(Random.Range(pc, 1f));
                    } else
                    {
                        SetKnob(Random.Range(0, pc));
                    }
                    break;
                case "GREATER THAN":
                    SetKnob(Random.Range(0f, pc));
                    break;
                case "LESS THAN":
                    SetKnob(Random.Range(pc, 1f));
                    break;
            }
        }
    }
    
    public void MoveKnob(float knobPosZ)
    {
        var leftLimitPos = leftLimit.position.z;
        var rightLimitPos = rightLimit.position.z;

        if (knobPosZ <= leftLimitPos)
        {
            knobPosZ = leftLimitPos;
        } else if (knobPosZ >= rightLimitPos)
        {
            knobPosZ = rightLimitPos;
        }

        Vector3 newPos = new Vector3(knob.transform.position.x, knob.transform.position.y, knobPosZ);
        
        knob.transform.position = newPos;

        percent = (knobPosZ - leftLimitPos) / (rightLimitPos - leftLimitPos);
        NotifyScenario();
    }

    private void SetKnob(float percent)
    {
        this.percent = percent;
        var range = rightLimit.position.z - leftLimit.position.z;
        var knobPosZ = leftLimit.position.z + range * percent;
        knob.transform.position = new Vector3(knob.transform.position.x, knob.transform.position.y, knobPosZ);
    }

    public string GetState()
    {
        return percent.ToString();
    }
}
