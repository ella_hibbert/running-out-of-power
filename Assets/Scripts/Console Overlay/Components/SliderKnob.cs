﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SliderKnob : MonoBehaviour {

    private SliderComponent slider;

    private Vector3 screenpoint;
    private Vector3 offset;
    private Camera consoleCamera;

    public void Init(SliderComponent slider)
    {
        ConsoleOverlay consoleParent = GetComponentInParent<ConsoleOverlay>();
        consoleCamera = consoleParent.consoleCamera;

        this.slider = slider;
    }

    void OnMouseDown()
    {
        screenpoint = consoleCamera.WorldToScreenPoint(transform.position);
        offset = transform.position - consoleCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenpoint.z));
    }

    void OnMouseDrag()
    {
        Vector3 cursorPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenpoint.z);
        Vector3 cursorPosition = consoleCamera.ScreenToWorldPoint(cursorPoint) + offset;
        
        slider.MoveKnob(cursorPosition.z);
    }
}
