﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PanelComponent : MonoBehaviour {

    private TextMeshPro text;
    private Scenario scenario;

    public void Init(string label)
    {
        text = GetComponentInChildren<TextMeshPro>();
        text.text = label;

        scenario = FindObjectOfType<Controller>().scenario;
    }

    public void NotifyScenario()
    {
        if (this is DialComponent)
        {
            scenario.DialInteraction(this as DialComponent);
        }
        else if (this is SwitchComponent)
        {
            scenario.SwitchInteraction(this as SwitchComponent);
        }
        else if (this is SliderComponent)
        {
            scenario.SliderInteraction(this as SliderComponent);
        }
        else if (this is ButtonComponent)
        {
            scenario.ButtonInteraction(this as ButtonComponent);
        }
    }
}
