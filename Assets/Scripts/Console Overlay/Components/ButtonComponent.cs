﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonComponent : PanelComponent {

    public GameObject mesh;
    public Transform pressableSection;
    public Color colour;

    private Material material;
    private Vector3 defaultPosition;

    public void InitSolution(ECManual.Solution solution, ECManual.ConsoleComponent component, bool required)
    {
        SkinnedMeshRenderer sMesh = mesh.GetComponent<SkinnedMeshRenderer>();
        material = new Material(sMesh.material);
        sMesh.material = material;

        var buttonComponent = component as ECManual.ButtonComponent;

        switch (buttonComponent.colour)
        {
            case ECManual.ButtonColour.BLUE:
                colour = Color.blue;
                break;
            case ECManual.ButtonColour.GREEN:
                colour = Color.green;
                break;
            case ECManual.ButtonColour.ORANGE:
                colour = new Color(1, 165/255f, 0);
                break;
            case ECManual.ButtonColour.PURPLE:
                colour = new Color(186/255f, 85 / 255f, 211 / 255f);
                break;
            case ECManual.ButtonColour.RED:
                colour = Color.red;
                break;
            case ECManual.ButtonColour.WHITE:
                colour = Color.white;
                break;
            case ECManual.ButtonColour.YELLOW:
                colour = Color.yellow;
                break;

        }

        Debug.Log("This button has colour " + buttonComponent.colour);

        material.SetColor("_EmissionColor", colour * 0.4f);

        defaultPosition = pressableSection.localPosition;

        Init(component.label);
    }
    
    void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 pressedLocation = new Vector3(pressableSection.localPosition.x, pressableSection.localPosition.y, pressableSection.localPosition.z - 0.00025f);
            pressableSection.localPosition = pressedLocation;
            NotifyScenario();
        }

        if (Input.GetMouseButtonUp(0))
        {
            pressableSection.localPosition = defaultPosition;
        }
    }

    void OnMouseExit()
    {
        pressableSection.localPosition = defaultPosition;
    }
}