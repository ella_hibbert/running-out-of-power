﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchComponent : PanelComponent {

    public Transform toggle;

    private bool on = true;
    private Quaternion upRotation;

    public void InitSymptom(ECManual.Symptom symptom, ECManual.ConsoleComponent component, bool isSymptomatic)
    {
        upRotation = toggle.localRotation;
        
        Init(component.label);

        if (isSymptomatic)
        {
            if (symptom.state.ToUpper() == "FALSE")
            {
                TurnOff();
            }
        } else
        {
            if (symptom.state.ToUpper() == "TRUE")
            {
                TurnOff();
            }
        }
    }
    
    void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (on)
            {
                TurnOff();
            } else
            {
                TurnOn();
            }
            
            NotifyScenario();
        }
    }

    private void TurnOff()
    {
        var newRot = upRotation;
        newRot.eulerAngles = new Vector3(0, 0, 60);
        toggle.localRotation = newRot;
        on = false;
    }

    private void TurnOn()
    {
        toggle.localRotation = upRotation;
        on = true;
    }

    public string GetState()
    {
        return on ? "On" : "Off";
    }
}
