﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DialComponent : PanelComponent
{
    public Transform rotatingDial;

    private string currentSection;

    private Quaternion redRotation;
    private Quaternion yellowRotation;
    private Quaternion greenRotation;
    
    public void InitSymptom(ECManual.Symptom symptom, ECManual.ConsoleComponent component, bool isSymptomatic)
    {
        redRotation = rotatingDial.localRotation;
        yellowRotation.eulerAngles = new Vector3(210, -90, -90);
        greenRotation.eulerAngles = new Vector3(315, -90, -90);
        
        Init(component.label);

        if (isSymptomatic)
        {
            MoveDial(symptom.state);
        } else
        {
            var possibleStates = new string[] { "RED", "YELLOW", "GREEN" };

            var randomState = possibleStates.Where(x => x != symptom.state.ToUpper()).ToList()[Random.Range(0, 2)];

            MoveDial(randomState);
        }

    }

    private void MoveDial(string section)
    {
        section = section.ToUpper();

        if (section == "RED")
        {
            rotatingDial.localRotation = redRotation;
        }
        else if (section == "YELLOW")
        {
            rotatingDial.localRotation = yellowRotation;
        }
        else if (section == "GREEN")
        {
            rotatingDial.localRotation = greenRotation;
        }

        currentSection = section;
    }
    
    public void ChangeTo(string section)
    {
        MoveDial(section);

        NotifyScenario();
    }

    public string GetState()
    {
        return currentSection;
    }

}
