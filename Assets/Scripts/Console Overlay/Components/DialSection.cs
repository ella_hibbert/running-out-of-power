﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialSection : MonoBehaviour {

    public DialComponent dial;

    void OnMouseDown()
    {
        dial.ChangeTo(name);
    }
}
