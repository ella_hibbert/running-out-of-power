﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ConsoleOverlay : MonoBehaviour {

    public Camera consoleCamera;
    public string roomName;
    public Transform[] panelPositions = new Transform[8];

    public PanelDatabase panelDb;
    public Panel[] panels = new Panel[8];
    public PanelType[] selectedPanelTypes;

    public void Init(List<ECManual.Problem> expressedProblems, List<ECManual.Problem> nonExpressedProblems, ECManual.Console consoleDefiniton)
    {
        Debug.Log(name + " needs to process PRESENT problems:" + expressedProblems.Count + ", and NOT PRESENT problems: " + nonExpressedProblems.Count);
        
        var consoleSymptoms = expressedProblems.SelectMany(z => z.symptoms.Where(x => x.source is ECManual.ConsoleComponent)).ToList();
        var consoleNonSymptoms = nonExpressedProblems.SelectMany(z => z.symptoms.Where(x => x.source is ECManual.ConsoleComponent)).ToList();
        var consoleSolutions = expressedProblems.SelectMany(z => z.solutions.Where(x => x is ECManual.Solution)).ToList();
        var consoleNonSolutions = nonExpressedProblems.SelectMany(z => z.solutions.Where(x => x is ECManual.Solution)).ToList();

        for (int i = 0; i < consoleDefiniton.panels.Length; i++)
        {
            var panelDefinition = consoleDefiniton.panels[i];
            var panelPrefab = panelDb.GetPanel(panelDefinition.type, panelDefinition.components.Length);
            
            var panel = Instantiate(panelPrefab);

            panels[i] = panel;
            panel.transform.SetParent(panelPositions[i], false);
            panel.transform.localPosition = Vector3.zero;
            panel.Init();

            // first pass to cover all diagnostic components
            for (int j = 0; j < panelDefinition.components.Length; j++)
            {
                var componentDefiniton = panelDefinition.components[j];
                
                var symptom = consoleSymptoms.FirstOrDefault(x => ((ECManual.ConsoleComponent)x.source).label == componentDefiniton.label);

                if (symptom != null)
                {
                    //Debug.Log(componentDefiniton.label + " NEEDS TO BE SET ACCORDING TO A PROBLEM!!");

                    SetComponent(panel.components[j], symptom, true);
                }
                else
                {
                    var nonSymptom = consoleNonSymptoms.FirstOrDefault(x => ((ECManual.ConsoleComponent)x.source).label == componentDefiniton.label);

                    if (nonSymptom != null)
                    {
                        //Debug.Log(componentDefiniton.label + " needs to be set so that it isn't showing a problem!!");

                        SetComponent(panel.components[j], nonSymptom, false);
                    } else
                    {
                        // this is not any kind of symptomatic component
                    }
                }
            }

            // second pass to cover all interactive components
            for (int j = 0; j < panelDefinition.components.Length; j++)
            {
                var componentDefiniton = panelDefinition.components[j];

                var solution = consoleSolutions.FirstOrDefault(x => (x.component.label == componentDefiniton.label));

                if (solution != null)
                {
                    Debug.Log(solution.component.label + " " + solution.component.type + " IS involved in a solution for an existing problem");

                    SetComponent(panel.components[j], solution, true);

                } else
                {
                    var nonSolution = consoleNonSolutions.FirstOrDefault(x => (x.component.label == componentDefiniton.label));

                    if (nonSolution != null)
                    {
                        Debug.Log(nonSolution.component.label + " " + nonSolution.component.type + " is NOT involved in a solution for an existing problem");

                        SetComponent(panel.components[j], nonSolution, false);
                    } else
                    {
                        // this component is not interactive
                    }
                }
            }
        }
    }

    private void SetComponent(PanelComponent component, ECManual.Symptom symptom, bool isSymptomatic)
    {
        var source = (ECManual.ConsoleComponent)symptom.source;

        switch (source.type)
        {
            case ECManual.ConsoleComponentType.DIAL:
                var dialComponent = component as DialComponent;
                dialComponent.InitSymptom(symptom, source, isSymptomatic);
                break;
            case ECManual.ConsoleComponentType.SLIDER:
                var sliderComponent = component as SliderComponent;
                sliderComponent.InitSymptom(symptom, source, isSymptomatic);
                break;
            case ECManual.ConsoleComponentType.SWITCH:
                var switchComponent = component as SwitchComponent;
                switchComponent.InitSymptom(symptom, source, isSymptomatic);
                break;
        }
    }

    private void SetComponent(PanelComponent component, ECManual.Solution solution, bool required)
    {
        switch (solution.component.type)
        {
            //case ECManual.ConsoleComponentType.DIAL:
            //    var dialComponent = component as DialComponent;
            //    dialComponent.InitSolution(symptom, source, isSymptomatic);
            //    break;
            //case ECManual.ConsoleComponentType.SLIDER:
            //    var sliderComponent = component as SliderComponent;
            //    sliderComponent.InitSolution(symptom, source, isSymptomatic);
            //    break;
            //case ECManual.ConsoleComponentType.SWITCH:
            //    var switchComponent = component as SwitchComponent;
            //    switchComponent.InitSolution(symptom, source, isSymptomatic);
            //    break;
            case ECManual.ConsoleComponentType.BUTTON:
                var buttonComponent = component as ButtonComponent;
                buttonComponent.InitSolution(solution, solution.component, required);
                break;
        }
    }
    
    public void Show()
    {
        consoleCamera.gameObject.SetActive(true);
    }

    public void Hide()
    {
        consoleCamera.gameObject.SetActive(false);
    }

    public bool IsVisible()
    {
        return consoleCamera.gameObject.activeInHierarchy;
    }
}
