﻿using ECManual;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Controller : MonoBehaviour {
    
    public RoomLoader roomLoader;
    
    public PlayerMovement playerMovement;
    public GameObject keyboardPrompt;

    private CameraMover cameraMover;
    
    private IHasProximityCollider objNextToPlayer;

    public Room startingRoom;
    private Room currentRoom;
    
    public Scenario scenario;

    void Start()
    {
        var password = PlayerPrefs.GetString("password");
        PlayerPrefs.DeleteKey("password");

        // in case we skipped setting the password
        if (string.IsNullOrEmpty(password))
        {
            password = "errors";
        }

        cameraMover = Camera.main.GetComponent<CameraMover>();

        currentRoom = startingRoom;
        roomLoader.Init(currentRoom);

        var randomDoor = currentRoom.GetComponentInChildren<Door>();
        MoveToRoom(currentRoom, randomDoor.GetSpawnPoint());
        
        int seed = 0;
        string assetDirectory = Path.Combine(Application.streamingAssetsPath, "Data");
        Manual manual = new Manual(assetDirectory, seed);
        
        var scenarioBuilder = new ScenarioBuilder(password, manual.problemList);
        scenario = scenarioBuilder.GenerateScenario();

        MersenneTwister random = new MersenneTwister(seed);
        var consoles = FindObjectsOfType<ConsoleOverlay>();

        foreach (var console in consoles)
        {
            var expressedProblems = scenario.shipProblems.Where(x => x.Room == console.roomName).ToList();
            var expressedProblemDefinitions = manual.problems.Where(x => expressedProblems.Any(y => y.Name == x.name)).ToList();

            var nonExpressedProblemsDefinitions = manual.problems.Where(x => x.roomName.ToUpper() == console.roomName.ToUpper()).Where(x => expressedProblemDefinitions.FirstOrDefault(y => y.name == x.name) == null).ToList();

            var consoleDefiniton = manual.rooms.First(x => x.name.ToString().ToUpper() == console.roomName.ToUpper()).console;

            console.Init(expressedProblemDefinitions, nonExpressedProblemsDefinitions, consoleDefiniton);
        }
    }

    void Update()
    {
        // get interaction key
        if (Input.GetKeyDown(KeyCode.Space))
        {
            // check for a door to interact with
            if (objNextToPlayer != null)
            {
                objNextToPlayer.Use();
                return;
            }
        }
        
        playerMovement.DetectDirection();
    }
    
    void FixedUpdate()
    {
        playerMovement.Move();
    }
    
    public void PlayerInProximityTo(IHasProximityCollider obj, Transform transform)
    {
        if (obj != null)
        {
            var keyboardPromptLocation = obj.GetKeyboardPromptLocation();
            keyboardPrompt.SetActive(true);

            keyboardPrompt.transform.SetParent(keyboardPromptLocation, true);
            keyboardPrompt.transform.localPosition = Vector3.zero;
            keyboardPrompt.GetComponent<RectTransform>().localRotation = Quaternion.identity;
        } else
        {
            keyboardPrompt.SetActive(false);
        }
        
        objNextToPlayer = obj;
    }
    
    public void MoveToRoom(Room room, Transform spawnPosition)
    {
        // move the camera to the other room's camera position
        cameraMover.SetBoundariesAndPlace(room.leftCameraBoundary, room.rightCameraBoundary);
        
        currentRoom = room;
        roomLoader.SetCurrentRoom(currentRoom);

        // move the player to the other door's spawn position
        var newPos = new Vector3(spawnPosition.position.x, spawnPosition.position.y, spawnPosition.position.z);
        playerMovement.transform.position = newPos;
    }
}
