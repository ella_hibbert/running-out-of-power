﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMover : MonoBehaviour {

    private PlayerMovement player;
    private Transform leftBoundary;
    private Transform rightBoundary;

    private bool canMove;

	// Use this for initialization
	void Awake () {
        player = FindObjectOfType<PlayerMovement>();
	}

    public void SetBoundariesAndPlace(Transform leftBoundary, Transform rightBoundary)
    {
        this.leftBoundary = leftBoundary;
        this.rightBoundary = rightBoundary;

        canMove = !leftBoundary.Equals(rightBoundary);
        
        Camera.main.transform.position = new Vector3(leftBoundary.position.x, leftBoundary.position.y, player.transform.position.z);
        Camera.main.transform.rotation = leftBoundary.rotation;

        EnsureWithinBounds();
    }
	
	// Update is called once per frame
	void Update () {
        var screenPoint = Camera.main.WorldToScreenPoint(player.transform.position);
        
        if (canMove)
        {
            if (screenPoint.x < 250 || screenPoint.x > Screen.width - 250)
            {
                if (Camera.main.transform.position.z <= leftBoundary.position.z && Camera.main.transform.position.z >= rightBoundary.position.z)
                {
                    var playerXPos = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, player.transform.position.z);
                    var newCameraPos = Vector3.Lerp(Camera.main.transform.position, playerXPos, Time.deltaTime);

                    Camera.main.transform.position = newCameraPos;
                }
            }

            EnsureWithinBounds();
        }
	}

    private void EnsureWithinBounds()
    {
        if (Camera.main.transform.position.z > leftBoundary.position.z)
        {
            Camera.main.transform.position = leftBoundary.position;
        }
        else if (Camera.main.transform.position.z < rightBoundary.position.z)
        {
            Camera.main.transform.position = rightBoundary.position;
        }
    }
}
